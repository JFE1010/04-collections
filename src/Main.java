import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.TreeSet;

public class Main {

	public static void main(String[] args) {
		
		simpleArrayList();
		
		genericArrayList();
		
		overridingEqualsMethod();
		
		lambdaExpressions();
		
		convertingArrayListToArray();
		
		sortingUsingComparableInterface();
		
		sortingUsingComparatorInterface();
	}

	//-----------------------------------
	// 1. Simple ArrayList
	//-----------------------------------
	private static void simpleArrayList() {
		System.out.println("1: \n\n");
		
		// TODONE: create an ArrayList
		ArrayList list = new ArrayList();

		// TODONE: add "Foo" and "Bar" strings to your ArrayList
		list.add("Foo");
		list.add("Bar");
		
		// TODONE: Use the get(...) method to get the first element in the ArrayList as a String
		String str = (String)list.get(0);

		System.out.println(str);
		System.out.println("\n\n");
	}


	//-----------------------------------
	// 2. Generic ArrayList
	//-----------------------------------
	private static void genericArrayList() {
		System.out.println("2. \n\n");
		// TODONE: Create a Generic ArrayList of type String
		ArrayList<String> list = new ArrayList<>();
		
		// TODONE: Add "Foo" and "Bar" strings to the list
		list.add("Foo");
		list.add("Bar");
				
		// TODONE: Print out all the items in the list
		for (String str : list) {
			System.out.println(str);
		}
		
		// TODONE: Print out the size of the list
		System.out.println("List size: " + list.size());
		System.out.println("\n\n");
	}
	

	//-----------------------------------
	// 3. Overriding equals() method
	//-----------------------------------
	private static void overridingEqualsMethod() {
		System.out.println("3.\n\n");
		// TODONE: modify the Person class and override the equals method.
		//       Person objects with the same ssn should return true
		// HINT: In Spring Tool Suite, right click on the class name -> Source -> Override / Implement Methods
		
		
		// Adding some people to the person list
		ArrayList<Person> personList = new ArrayList<>();
		personList.add(new Person("John", "Doe", "555-55-5555"));
		personList.add(new Person("John", "Doe", "666-66-6666"));
				
		// Remove a person from the list
		Person personToRemove = new Person("John", "Doe", "555-55-5555");
		personList.remove(personToRemove);
		
		// Display people in the person list
		System.out.println("People in personList:");
		for(Person person : personList) {
			System.out.println(person);
		}
		System.out.println("\n\n");
	}

	//-----------------------------------
	// 4. Lambda expressions
	//-----------------------------------
	private static void lambdaExpressions() {
		System.out.println("4.\n\n");
		ArrayList<Person> personList = new ArrayList<>();
		personList.add(new Person("John", "Doe", "555-55-5555"));
		personList.add(new Person("Jane", "Doe", "777-77-7777"));
		System.out.println("People in list2:");
		
		// TODONE: use the forEach method on the personList and a lamba expression to display
		// all the people in the personList
		personList.forEach(i -> System.out.println(i));
		System.out.println("\n\n");
	}
	
	//-----------------------------------
	// 5. Converting ArrayList to an Array
	//-----------------------------------
	private static void convertingArrayListToArray() {
		System.out.println("5.\n\n");

		// Constructing an ArrayList of Persons
		ArrayList<Person> personList = new ArrayList<>();
		personList.add(new Person("John", "Doe", "555-55-5555"));
		personList.add(new Person("Jane", "Doe", "777-77-7777"));
		
		// TODONE: Convert the personList to an array and then display the people
		Person[] peopleArray = personList.toArray(new Person[0]);
		for (Person person : peopleArray) {
			System.out.println(person);
		}
		System.out.println("\n\n");
	}
	
	//-----------------------------------
	// 6. Sorting using Comparable interface
	//-----------------------------------
	private static void sortingUsingComparableInterface() {
		System.out.println("6.\n\n");
		// TODONE: implement the Comparable<> interface on the Fruit class and compare by the fruit name
		TreeSet<Fruit> fruits = new TreeSet<>();
		fruits.add(new Fruit("Orange"));
		fruits.add(new Fruit("Apple"));
		fruits.add(new Fruit("Pear"));
		fruits.add(new Fruit("Pineapple"));
		System.out.println("Fruits:");
		for(Fruit fruit : fruits) {
			System.out.println(fruit);
		}
		System.out.println("\n\n");
	}
	
	//-----------------------------------
	// 7. Sorting using the Comparator interface
	//-----------------------------------

	private static void sortingUsingComparatorInterface() {
		System.out.println("7.\n\n");
		// TODONE: Create a new class PersonComparator and implement the Comparator interface
		// to compare by the person's last name
		TreeSet<Person> sortablePersonList = new TreeSet<>(new PersonComparator());
		sortablePersonList.add(new Person("Ted", "Zeiler", "777-77-7777"));
		sortablePersonList.add(new Person("John", "Doe", "555-55-5555"));
		sortablePersonList.add(new Person("Jane", "Day", "777-77-7777"));
		System.out.println("Sortable Person List: ");
		for(Person person : sortablePersonList) {
			System.out.println(person);
		}
		System.out.println("\n\n");
	}

}